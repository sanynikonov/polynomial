using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PolynomialObject.Exceptions
{
    [Serializable]
    public class PolynomialArgumentException : Exception
    {

        public PolynomialArgumentException() { }

        public PolynomialArgumentException(string message) { }
    }
}
